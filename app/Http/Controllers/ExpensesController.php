<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Expense;
use App\Models\Tip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ExpensesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total = auth()->user()->total_drafts;
        $expenses = Expense::where('user_id', auth()->user()->id)->with('category')->orderBy('id', 'desc')->paginate(10);
        $tip = Tip::orderByRaw('RAND()')->first(); //inRandomOrder()->first(); //
        $categories = Category::orderBy('id', 'desc')->get();;

        return view('cabinet.expenses', compact('expenses','tip', 'categories', 'total'));
    }
    // compact('expences')
    // ['expences'=>$expences]

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'price'=>'required|numeric',
            'category'=>'required|integer',

        ]);

        $expense = Expense::create([
            'user_id'=>Auth::user()->id,
            'category_id'=>$request->category,
            'price'=> $request->price,
            'description'=> $request->description ? $request->description : 'Unknown',
        ]);

        if($expense){
            Session::flash('success', 'Your expense has saved');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expense $expense)
    {
        //TODO: create update expenses
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        if(Expense::destroy($expense->id)){
            Session::flash('error', 'Your record has been deleted');
            return redirect()->back();
        }
    }


//---------------------------------------------------------------

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
}
