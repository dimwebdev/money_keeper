<?php

namespace App\Http\Controllers;

use App\Models\Draft;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use function PHPSTORM_META\type;

class DraftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $total = auth()->user()->total_drafts;

        $incomes = Draft::where('user_id', auth()->user()->id)->where('type', 'income')->orderBy('price', 'DESC')->get();
        $spending = Draft::where('user_id', auth()->user()->id)->where('type', 'spending')->orderBy('price', 'DESC')->get();

        return view('cabinet.drafts', compact('incomes', 'spending', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type'=>'required|string',
            'price'=>'required|numeric',
            'description'=>'required|string',
        ]);

        $draft = Draft::create([
            'user_id'=>Auth::user()->id,
            'type'=>$request->type,
            'price'=> $request->price,
            'description'=> $request->description,
        ]);

        if($draft){
            Session::flash('success', 'New record has been saved');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function show(Draft $draft)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function edit(Draft $draft)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Draft $draft)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function destroy(Draft $draft)
    {
        if(Draft::destroy($draft->id)){
            Session::flash('error', 'Your record has been deleted');
            return redirect()->back();
        }else{
            Session::flash('error', 'Some error has occurred');
            return redirect()->back();
        }
    }
}
