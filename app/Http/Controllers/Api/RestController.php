<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RestController extends Controller
{
    public function getStatistic(User $user){
        $recordSet = $user->expenses()
            ->select('category_id', DB::raw('SUM(price) as totalPrice'))
            ->groupBy('category_id')
            ->orderBy('totalPrice', 'DESC')
            ->get();

        $labels = [];
        $data = [];

        foreach ($recordSet as $record){
            $cat = Category::select('name')->find($record->category_id);
            $labels [] = $cat->name;
            $data [] = $record->totalPrice;
        }

        $statisticData = [
                'labels' => $labels,
                'datasets' => array([
                            'label' => 'Expenses categories',
                            'data' =>  $data,
                            'backgroundColor' => [
                                'rgba(255, 99, 132, 0.9)',
                                'rgba(54, 162, 235, 0.9)',
                                'rgba(255, 206, 86, 0.9)',
                                'rgba(75, 192, 192, 0.9)',
                                'rgba(153, 102, 255, 0.9)',
                                'rgba(255, 159, 64, 0.9)'
                            ],
                            'borderColor' => [
                                'rgba(255,99,132,1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            'border' => 1,
//                            'backgroundColor' => '#f87979',

                        ])

        ];
        return $statisticData;
//        return json_encode($statisticData);
    }
}
