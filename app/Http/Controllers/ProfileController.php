<?php

namespace App\Http\Controllers;

use App\Models\Draft;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::where('id','=', auth()->user()->id)->first();

        return view('cabinet.profile', compact('profile'));
    }





    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {

        $request->validate([
            "firstname" => 'required|string|between:2,255',
            "lastname" => 'required|string|max:255',
            "currency" => 'required|string',
            "language" => 'required|string',
        ]);

        $profile->firstname = $request->firstname;
        $profile->lastname = $request->lastname;
        $profile->currency = $request->currency;
        $profile->language = $request->language;

        if($profile->save()){
            Session::flash('success','Your date is updated');
            return redirect()->back();
        }else{
            Session::flash('error','Your date was not updated');
            return redirect()->back();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
