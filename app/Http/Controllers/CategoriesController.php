<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return view('cabinet.categories', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'icon'=>'required|string',
            'name'=>'required|string',
            'description'=>'required|string',
        ]);

        $category = Category::create([
            'icon'=>$request->icon,
            'name'=> $request->name,
            'description'=> $request->description,
        ]);

        if($category){
            Session::flash('success', 'New record has been saved');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('cabinet.category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        if($request->name == $category->name){
            $request->validate([
                "icon" => 'required|string|between:2,255',
                "name" => 'required|string|max:255',
                "description" => 'required|string|max:255',
            ]);
        }else{
            $request->validate([
                "icon" => 'required|string|between:2,255',
                "name" => 'required|string|unique:categories|max:255',
                "description" => 'required|string|max:255',
            ]);
        }


        $category->icon = $request->icon;
        $category->name = $request->name;
        $category->description = $request->description;

        if($category->save()){
            Session::flash('success','Your date is updated');
            return redirect()->back();
        }else{
            Session::flash('error','Your date was not updated');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if(Category::destroy(1000000000)){
            Session::flash('error', 'Your category has been deleted');
            return redirect()->back();
        }else{
            Session::flash('error', "Sorry, you don't have permission to deleting");
            return redirect()->back();
        }
    }
}
