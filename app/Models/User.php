<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * This is relation for profile
     *
     */
    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function drafts(){
        return $this->hasMany(Draft::class);
    }

    public function expenses(){
        return $this->hasMany(Expense::class);
    }

    public function getTotalDraftsAttribute(){
        $total = collect(['incomes', 'spending', 'balance']);

        $incomes  =  Draft::where('user_id', auth()->user()->id)->where('type', 'income')->sum('price');
        $spending =  Draft::where('user_id', auth()->user()->id)->where('type', 'spending')->sum('price');
        $balance  =  $incomes - $spending;

        $total = $total->combine([ $incomes, $spending , $balance]);

        return $total;
    }

    public function getStatisticAttribute(){

    }

}
