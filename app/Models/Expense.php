<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'user_id', 'category_id', 'price', 'description'
    ];

    public function category(){
        return $this->belongsTo(Category::class,  'category_id','id');
    }

}
