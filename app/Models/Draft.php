<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Draft extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','type','name','price','description'
    ];

    public function owner(){
        return $this->belongsTo(Draft::class);
    }
}
