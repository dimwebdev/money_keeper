<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix'=>'cabinet', 'middleware'=>['auth']], function(){
    Route::resource('expenses', 'ExpensesController')->only( 'index', 'store', 'destroy');
    Route::resource( 'profile', 'ProfileController'     )->only('index', 'update', 'destroy');
    Route::resource( 'drafts', 'DraftsController'       )->only('index', 'store', 'destroy');
    Route::resource( 'categories', 'CategoriesController'  )->only('index', 'edit', 'update', 'store', 'destroy');
    Route::resource( 'statistic', 'StatisticController' )->only('index', 'update', 'destroy');
});

