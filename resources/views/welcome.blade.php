<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon.png') }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                /*color: #636b6f;*/
                color: #2C2B19;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                min-height: 100vh;
                margin: 0;
            }
            body{
                background-image: url("{{ asset('img/homebg.jpg') }}");
                background-size: cover;
            }

            .full-height {
                min-height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                background-color: rgba(255,255,255,0.4);
                border-radius: 10px;
                width: 75%;
                margin-left: auto;
                margin-right: auto;
                padding: 2em 0;
            }

            .title {
                font-size: 2.5em;
                line-height: 1em;
            }

            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .messag {
                  padding: 0 2em;
                  font-size: 1.5em;
                  /*font-weight: 600;*/
                  letter-spacing: .1rem;
              }

            .m-b-md {
                margin-bottom: 30px;
            }
            .btn{
                text-decoration: none;
                color: white;
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #fff;
                background-color: #007bff;
                border-color: #007bff;
            }
            .btn:hover {
                color: #fff;
                background-color: #0069d9;
                border-color: #0062cc;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/cabinet/expenses') }}">Cabinet</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Welcome to the <br>
                    money keeper site!<br>
                    <small><small>(beta version)</small></small>
                </div>

               <p class="messag">
                   We will help you plan your budget, keep track of your expenses.
                   You will be able to monitor spending  and adjust them.
               </p>
                <a href="/cabinet/expenses" class="btn">Go to the private cabinet</a>
            </div>
        </div>
    </body>
</html>
