<aside class="col-4 main_sidebar">
    <ul>
        <li class="{{ Request::is('*/profile') ? "active" : "" }}"><i class="fa fa-user-circle"></i><a href="/cabinet/profile">{{ __('Profile') }}</a></li>
        <hr>
        <br>
        {{--<li @if(Request::is('*/expenses') )class="active"@endif><i class="fa fa-money"></i><a href="/cabinet/expenses">{{ __('My expenses') }}</a></li>--}}
        <li class="{{ Request::is('*/expenses') ? "active" : "" }}"><i class="fa fa-money"></i><a href="/cabinet/expenses">{{ __('My expenses') }}</a></li>
        <li class="{{ Request::is('*/drafts') ? "active" : "" }}"><i class="fa fa-cart-plus"></i><a href="/cabinet/drafts">{{ __('Incomes and drafts') }}</a></li>
        <li class="{{ Request::is('*/category') ? "active" : "" }}"><i class="fa fa-bicycle"></i><a href="/cabinet/categories">{{ __('Category') }}</a></li>
        <li class="{{ Request::is('*/statistic') ? "active" : "" }}"><i class="fa fa-table"></i><a href="/cabinet/statistic">{{ __('Statistic') }}</a></li>
    </ul>
</aside>

