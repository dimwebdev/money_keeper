@extends('layouts.app')

@section('content')


    <main>
        @include('parts.message')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">

                    <div class="card" >
                        <div class="card-header">{{ __('Categories') }}</div>
                        <div class="card-body">
                            <table class="table table-striped expenses-list">
                                <thead>
                                <tr>
                                    <th scope="col-3">{{ __('#') }}</th>
                                    <th scope="col-3">{{ __('icon') }}</th>
                                    <th scope="col-3">{{ __('name') }}</th>
                                    <th scope="col-3">{{ __('Description') }}</th>
                                    <th scope="col-3">{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><i class="{{ $category->icon }}" style="font-size:2em;"></i></td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->description }}</td>
                                        <td>
                                            <a href="/cabinet/categories/{{ $category->id }}/edit" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>

                                            <form action="/cabinet/categories/{{ $category->id }}" style="display: inline-block" method="post">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div> <!-- Card -->
                </div><!-- end .col -->

            </div><!-- end .row -->
        </div><!-- end .container -->

        <!-- Modal -->
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success add-expense-button" data-toggle="modal" data-target="#exampleModal">
            {{ __('+ Add new') }}
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create new category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="bg-red">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form method="post" action="/cabinet/categories">
                            @csrf
                            <div class="container form-group">

                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Font awesome icon name</span>
                                </div>
                                <input type="text" name="icon" class="form-control" placeholder="fa fa-bicycle">
                            </div>

                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Unique category name</span>
                                </div>
                                <input type="text" class="form-control" name="name" placeholder="Name">
                            </div>

                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Category description</span>
                                </div>
                                <input type="text" class="form-control" name="description" placeholder="Description">
                            </div>

                            <div class="text-center form-group">
                                <button type="submit" class="btn btn-primary">Store</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Modal -->
    </main>
@endsection
