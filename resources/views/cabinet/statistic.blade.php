@extends('layouts.app')

@section('content')


    <main>
        @include('parts.message')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <h1>Category statistic</h1>
                    <chart-bar :user="{{ auth()->user()->id }}"></chart-bar>

                </div><!-- end .col -->

            </div><!-- end .row -->
        </div><!-- end .container -->

    </main>
@endsection
