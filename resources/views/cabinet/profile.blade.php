@extends('layouts.app')

@section('content')


    <main>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card" >
                        <div class="card-header">{{ __('Update your data') }}</div>

                        <div class="card-body">
                            @include('parts.message')

                            <form method="POST" action="/cabinet/profile/{{ auth()->user()->profile->id }}">
                                {{ method_field('PUT') }}
                                @csrf

                                <div class="form-group row">
                                    <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('First name') }}</label>

                                    <div class="col-md-6">
                                        <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ $profile->firstname }}" required>

                                        @if ($errors->has('firstname'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Last name') }}</label>

                                    <div class="col-md-6">
                                        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ $profile->lastname }}" required>

                                        @if ($errors->has('lastname'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="currency" class="col-md-4 col-form-label text-md-right">{{ __('Select currency') }}</label>

                                    <div class="col-md-6">
                                        <select name="currency" id="currency" class="form-control{{ $errors->has('currency') ? ' is-invalid' : '' }}" required>
                                            <option value="usd" @if($profile->currency == 'usd')selected @endif >USD</option>
                                            <option value="eur" @if($profile->currency == 'eur')selected @endif >EUR</option>
                                            <option value="uah" @if($profile->currency == 'uah')selected @endif>UAH</option>
                                        </select>

                                        @if ($errors->has('currency'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('currency') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="language" class="col-md-4 col-form-label text-md-right">{{ __('Select language') }}</label>

                                    <div class="col-md-6">
                                        <select name="language" id="language" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }}" required>
                                            <option value="en" @if($profile->language == 'en') selected @endif >English</option>
                                            <option value="ukr" @if($profile->language == 'ukr') selected @endif >Ukrainian</option>
                                            <option value="rus" @if($profile->language == 'rus') selected @endif >Russian</option>
                                        </select>

                                        @if ($errors->has('language'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('language') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Update') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
