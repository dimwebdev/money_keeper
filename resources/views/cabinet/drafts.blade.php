@extends('layouts.app')

@section('content')


    <main>
        @include('parts.message')
        <div class="container">
            <div class="row justify-content-center form-group p-3 bg-info">
                <div class="col-md-4 text-center">{{ __('Your monthly incomes: ') }} <strong>{{ $total['incomes'] }}</strong> </div>
                <div class="col-md-4 text-center">{{ __('Your planing monthly spending: ') }} <strong>{{ $total['spending'] }}</strong> </div>
                <div class="col-md-4 text-center">{{ __('Your monthly balance: ') }} <strong>{{ $total['balance'] }}</strong> </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">

                    <div class="card" >
                        <div class="card-header">{{ __('Monthly incomes') }}</div>
                        <div class="card-body">
                            <table class="table table-striped expenses-list">
                                <thead>
                                <tr>
                                    <th scope="col-3">{{ __('Price') }}</th>
                                    <th scope="col-3">{{ __('Description') }}</th>
                                    <th scope="col-3">{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($incomes as $income)
                                    <tr>
                                        <td>{{ $income->price .' '. auth()->user()->profile->currency}}</td>
                                        <td>{{ $income->description }}</td>
                                        <td>
                                            <form action="/cabinet/drafts/{{ $income->id }}" style="display: inline-block" method="post">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div> <!-- Card -->
                </div>
                <div class="col-md-6">
                    <div class="card" >
                        <div class="card-header">{{ __('Planning monthly payments') }}</div>

                        <div class="card-body">
                            <table class="table table-striped expenses-list">
                                <thead>
                                <tr>
                                    <th scope="col-3">{{ __('Price') }}</th>
                                    <th scope="col-3">{{ __('Description') }}</th>
                                    <th scope="col-3">{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($spending as $item)
                                    <tr>
                                        <td>{{ $item->price .' '. auth()->user()->profile->currency}}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            <form action="/cabinet/drafts/{{ $item->id }}" style="display: inline-block" method="post">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <button class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end .container -->

        <!-- Modal -->
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success add-expense-button" data-toggle="modal" data-target="#exampleModal">
            {{ __('+ Add new') }}
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Write down your purchase</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="bg-red">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form method="post" action="/cabinet/drafts">
                            @csrf
                            <div class="container form-group">
                                <div class="row">
                                    <div class="col-md-5 text-right no-gutters">
                                        <label>
                                            <input type="radio" name="type" value="income"  class="modal-radio" checked>
                                            <img src="{{ asset('img/btn-income.png') }}" alt="">
                                        </label>
                                    </div>

                                    <div class="col-md-2 text-center" style="font-size: 2em;"> OR </div>

                                    <div class="col-md-5 text-left no-gutters">
                                        <label class="">
                                            <input type="radio" name="type" class="modal-radio" value="spending">
                                            <img src="{{ asset('img/btn-spending.png') }}" alt="">
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                    <span class="input-group-text">0.00</span>
                                </div>
                                <input type="text" name="price" class="form-control" aria-label="Amount (to the nearest dollar)">
                            </div>

                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Desctiption</span>
                                </div>
                                <input type="text" class="form-control" name="description">

                            </div>

                            <div class="text-center form-group">
                                <button type="submit" class="btn btn-primary">Store</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Modal -->
    </main>
@endsection
