@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="col tips text-center">{{ $tip->content }} </div>

            </div>
            @if ($errors->any())
                <div class="row text-center">
                    <div class="alert alert-danger w-100">
                        <h3>{{ __('Recording has not occurred') }}</h3>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <div class="row justify-content-center form-group p-3 bg-info">
                <div class="col-md-4 text-center">{{ __('Your monthly incomes: ') }} <strong>{{ $total['incomes'] }}</strong> </div>
                <div class="col-md-4 text-center">{{ __('Your planing monthly spending: ') }} <strong>{{ $total['spending'] }}</strong> </div>
                <div class="col-md-4 text-center">{{ __('Your monthly balance: ') }} <strong>{{ $total['balance'] }}</strong> </div>
            </div>
            <div class="row">
                @include('parts.message')
                <h3 class="text-center">Expenses</h3>
                    <table class="table table-striped expenses-list">
                        <thead>
                            <tr>
                                <th scope="col-3">{{ __('Date') }}</th>
                                <th scope="col-3">{{ __('Category') }}</th>
                                <th scope="col-3">{{ __('Price') }}</th>
                                <th scope="col-3">{{ __('Description') }}</th>
                                <th scope="col-3">{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($expenses as $expense)

                            <tr>
                                <td>{{ $expense->created_at->diffForHumans() }}</td>
                                <td><i class="{{ $expense->category->icon }}" style="font-size:2em;"></i></td>
                                <td>{{ $expense->price .' '. auth()->user()->profile->currency}}</td>
                                <td>{{ $expense->description }}</td>
                                <td>
                                    <form action="/cabinet/expenses/{{ $expense->id }}" style="display: inline-block" method="post">
                                        {{ method_field('DELETE') }}
                                        @csrf
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                {{ $expenses->links() }}

                <!-- Modal -->
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success add-expense-button" data-toggle="modal" data-target="#exampleModal">
                    {{ __('+ Add new expense') }}
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Write down your purchase</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true" class="bg-red">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form method="post" action="/cabinet/expenses">
                                    @csrf
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                            <span class="input-group-text">0.00</span>
                                        </div>
                                        <input type="text" name="price" class="form-control" aria-label="Amount (to the nearest dollar)">
                                    </div>
                                    <div class="container form-group">
                                        <div class="row">
                                            @foreach($categories as $category)
                                            <div class="col text-center">
                                                <label class="">
                                                    <input type="radio" name="category" value="{{ $category->id }}" class="modal-radio" checked>
                                                    <i class="fa {{ $category->icon }}" alt="{{ $category->name }}"></i>
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="input-group form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="">Desctiption</span>
                                        </div>
                                        <input type="text" class="form-control" name="description">

                                    </div>

                                    <div class="text-center form-group">
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Modal -->
            </div>
        </div>


    </main>
@endsection


