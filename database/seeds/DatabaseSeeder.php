<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Category::class,20)->create();
        factory(\App\Models\Tip::class,20)->create();

        factory(\App\Models\User::class, 10)->create()->each(function($user){
            $user->profile()->save(factory(\App\Models\Profile::class)->make());
            $user->expenses()->saveMany(factory(\App\Models\Expense::class,10)->make());
            $user->drafts()->saveMany(factory(\App\Models\Draft::class,10)->make());
        });
    }
}
