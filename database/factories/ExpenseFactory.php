<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Expense::class, function (Faker $faker) {
    return [
//        'user_id'=>$faker->numberBetween(0,10),
        'category_id'=>$faker->numberBetween(1,10),
//        'price'=> $faker->randomElement(['33.57',48.99, 37.95]),
        'price'=> $faker->randomFloat(2, 10, 1500),
        'description'=>$faker->realText(200),
    ];
});
