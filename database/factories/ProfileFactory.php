<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Profile::class, function (Faker $faker) {
    return [
//        'user_id'=>$faker->numberBetween(0,10),
        'firstname'=>$faker->firstName,
        'lastname'=>$faker->lastName,
        'language'=>$faker->randomElement(['en','rus', 'ukr']),
        'currency'=>$faker->randomElement(['usd', 'eur', 'uah']),
    ];
});
