<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
    return [
        'name'=> $faker->streetName(),
        'description'=> $faker->realText(30),
        'icon'=>$faker->imageUrl(70, 70, 'cats'),
    ];
});
