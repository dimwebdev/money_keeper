<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Tip::class, function (Faker $faker) {
    return [
        'title' =>  $faker->name(),
        'content' => $faker->realText(50),
    ];
});
