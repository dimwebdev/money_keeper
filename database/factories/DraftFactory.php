<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Draft::class, function (Faker $faker) {
    return [
//        'user_id'=>$faker->numberBetween(0,10),
        'type'=>$faker->randomElement(['income','spending']),
        'price'=> $faker->randomFloat(2, 10, 1500),
        'description'=>$faker->realText(200),
    ];
});
